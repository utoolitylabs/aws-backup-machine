# aws-backup-machine (backmac)

to make a template, run the following command from the repo root (requires cloudtoken or aws cli to be configured)

install cfn_flip via pip:
```commandline
pip install cfn_flip
```

run the package.sh script optionally stating the region for the template to work in (defaults to us-east-1)
```commandline
./package.sh us-east-2
```

This will zip and upload all the lambdas to buckets in each region, then will run a cloudformation package command on the template which replaces placeholders with s3 uris in the region you're deploying to (this doesn't work cross region).

Changes to parallel_sync will not be reflected in a running stack (even if it's updated), you'll need to restart the ec2 node.


### What it does

Backmac will add a Cloudwatch rule at the selected cron schedule to trigger the runbackups lambda. This will look for all Cloudformation stacks tagged with 'backmac_enabled=true' and add them to an SQS queue before triggering an execution of the State Machine.

The Cloudwatch rule will also trigger the snapshot cleaner, which will delete any snapshots older than their tagged 'backup_delete_after' date.