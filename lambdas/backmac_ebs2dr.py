import boto3
import pprint
import os


def ensure_ebssnap_complete(ebs_snap):
    print('Ensuring snapshot complete for: {}'.format(ebs_snap))
    ec2 = boto3.client('ec2', region_name=os.environ['AWS_REGION'])
    waiter = ec2.get_waiter('snapshot_completed')
    waiter.wait(SnapshotIds=[ebs_snap])
    return ()


def copy_ebssnap_todr(ebs_snap, rds_snap, dr_region):
    print('Copying snapshot {} {} to: {}'.format(ebs_snap, rds_snap, dr_region))
    ec2 = boto3.client('ec2', region_name=dr_region)
    try:
        dr_snap = ec2.copy_snapshot(
            SourceSnapshotId=ebs_snap, Description='dr-' + rds_snap, SourceRegion=os.environ['AWS_REGION']
        )
    except Exception as e:
        pprint.pprint(e)
    return dr_snap[u'SnapshotId']


def lambda_handler(event, context):
    stack_name = event['stack_name']
    ebs_snap = event['ebs_snap']
    rds_snap = event['rds_snap']
    dr_region = event['dr_region']
    ensure_ebssnap_complete(ebs_snap)
    dr_snap = copy_ebssnap_todr(ebs_snap, rds_snap, dr_region)
    event['ebs_dr_snap'] = dr_snap
    return event


if __name__ == "__main__":
    '''
        simulate lambda env vars
        '''
    os.environ['AWS_LAMBDA_LOG_GROUP_NAME'] = "/aws/lambda/awsbackup_lambda"
    os.environ['AWS_LAMBDA_LOG_STREAM_NAME'] = "2999/00/11/000 dummy stream name 000"
    os.environ['AWS_REGION'] = "us-east-2"

    event = {
        "backmac_instance": "i-00000000000000000",
        "ebs_backup_vol": "vol-00000000000000000",
        "dr_region": "us-east-1",
        "stack_name": "jira-stack",
        "security_group_name": "sg-abcdefgh",
        "rds_snap_arn": "arn:aws:rds:us-west-2:000000000000:snapshot:jira-stack-snap-000000000000",
        "rds_snap": "jira-stack-snap-000000000000",
        "ebs_snap": "snap-00000000000000000",
        "rsync_status": "Success",
        "rsync_command_id": "a753c655-f756-4795-b2b0-000000000000",
    }
    context = ''
    result = lambda_handler(event, context)
    pprint.pprint(result)
