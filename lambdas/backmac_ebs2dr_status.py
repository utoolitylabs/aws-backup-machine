import boto3
import pprint
import os
import time
from datetime import datetime, timedelta


def ensure_drsnap_complete(dr_snap, dr_region):
    print(f'Ensuring snapshot complete in {dr_region} for: {dr_snap}')
    ec2 = boto3.resource('ec2', region_name=dr_region)
    try:
        dr_snapshot = ec2.Snapshot(dr_snap)
    except Exception as e:
        print('type is:', e.__class__.__name__)
        pprint.pprint(e)
        template = "An exception of type {0} occurred. Arguments:\n{1!r}"
        message = template.format(type(e).__name__, e.args)
        return 'failed'
    return dr_snapshot.state


def tag_snapshot(dr_snap, stack_name, dr_region, backup_retention, extra_tags):
    print(f'Taging snapshot {dr_snap} for {stack_name} in: {dr_region}')
    ec2 = boto3.resource('ec2', region_name=dr_region)
    snapshot_obj = ec2.Snapshot(dr_snap)
    timestamp = time.strftime('%Y%m%d%H%M')
    tags = [
        {'Key': 'Name', 'Value': 'dr_' + stack_name + '_ebs_snap_' + timestamp},
        {'Key': 'backup_lambda_log_group_name', 'Value': f"{os.environ['AWS_LAMBDA_LOG_GROUP_NAME']}"},
        {'Key': 'backup_lambda_log_stream_name', 'Value': f"{os.environ['AWS_LAMBDA_LOG_STREAM_NAME']}"},
        {'Key': 'backup_date', 'Value': f'{datetime.now()}'},
        {'Key': 'backup_delete_after', 'Value': f'{datetime.now() + timedelta(backup_retention)}'},
    ]
    tags = tags + clean_tags(extra_tags)
    snapshot_obj.create_tags(Tags=tags)


def clean_tags(tags):
    # We don't want to overwrite the 'Name' tag, so remove it from the local dict.
    if len(tags) > 0:
        for tag in tags:
            if tag['Key'] == 'Name':
                del tag['Key']
    return tags


def lambda_handler(event, context):
    dr_snap = event['ebs_dr_snap']
    dr_region = event['dr_region']
    stack_name = event['stack_name']
    backup_retention = event['backup_retention']
    event['rds2dr_status'] = 'waiting'
    dr_snap_state = ensure_drsnap_complete(dr_snap, dr_region)
    if dr_snap_state == "completed":
        tag_snapshot(dr_snap, stack_name, dr_region, backup_retention, event['stack_tags'])
    event['ebs2dr_status'] = dr_snap_state
    return event


if __name__ == "__main__":
    '''
        simulate lambda env vars
        '''
    os.environ['AWS_LAMBDA_LOG_GROUP_NAME'] = "/aws/lambda/awsbackup_lambda"
    os.environ['AWS_LAMBDA_LOG_STREAM_NAME'] = "2999/00/11/000 dummy stream name 000"
    os.environ['AWS_REGION'] = "us-east-2"

    event = {
        "ebs_backup_vol": "vol-00000000000000000",
        "dr_region": "us-east-1",
        "stack_name": "jira-stack",
        "rds_snap_arn": "arn:aws:rds:us-west-2:000000000000:snapshot:jira-stack-snap-000000000000",
        "rds_snap": "jira-stack-snap-000000000000",
        "ebs_snap": "snap-00000000000000000",
        "ebs_dr_snap": "snap-00000000000000000",
    }
    context = ''
    result = lambda_handler(event, context)
    pprint.pprint(result)
