import boto3
import pprint
import os
import time
import datetime


def lambda_handler(event, context):
    stack_name = event['stack_name']
    ebs_backup_vol = event['ebs_backup_vol']
    backup_retention = event['backup_retention']
    timestamp = time.strftime('%Y%m%d%H%M')
    ebs_snap_name = stack_name + '-snap-' + timestamp
    ec2 = boto3.resource('ec2', region_name=os.environ['AWS_REGION'])
    ebs_vol = ec2.Volume(ebs_backup_vol)
    ebs_snap = ebs_vol.create_snapshot(
        Description="snapshot of " + stack_name + " ebs backup volume taken at " + timestamp
    )
    snapshot_obj = ec2.Snapshot(ebs_snap.id)
    snapshot_obj.create_tags(
        Tags=[
            {'Key': 'Name', 'Value': stack_name + '_ebs_snap_' + timestamp},
            {'Key': 'backup_lambda_log_group_name', 'Value': '{}'.format(os.environ['AWS_LAMBDA_LOG_GROUP_NAME'])},
            {'Key': 'backup_lambda_log_stream_name', 'Value': '{}'.format(os.environ['AWS_LAMBDA_LOG_STREAM_NAME'])},
            {'Key': 'backup_date', 'Value': '{}'.format(datetime.datetime.now())},
            {
                'Key': 'backup_delete_after',
                'Value': '{}'.format(datetime.datetime.now() + datetime.timedelta(backup_retention)),
            },
        ]
    )
    if len(event['stack_tags']) > 0:
        snapshot_obj.create_tags(Tags=event['stack_tags'])

    # add rds snapshot_id to event
    event['ebs_snap'] = ebs_snap.id
    pprint.pprint(event)
    return event


if __name__ == "__main__":
    '''
        simulate lambda env vars
        '''
    os.environ['AWS_LAMBDA_LOG_GROUP_NAME'] = "/aws/lambda/awsbackup_lambda"
    os.environ['AWS_LAMBDA_LOG_STREAM_NAME'] = "2999/00/11/[$LATEST]000 dummy stream name 000"
    os.environ['AWS_REGION'] = "us-east-2"

    event = {"stack_name": "jira-stack", "dr_region": "us-east-1", "ebs_backup_vol": "vol-00000000000000000"}
    context = ''
    result = lambda_handler(event, context)
    pprint.pprint(result)
