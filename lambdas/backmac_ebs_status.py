import boto3
import os


def ebs_state(ebs_vol):
    print(f'Check volume state for: {ebs_vol}')
    ec2 = boto3.client('ec2', region_name=os.environ['AWS_REGION'])
    ebs_volume_status = ec2.describe_volumes(VolumeIds=[ebs_vol])
    return ebs_volume_status['Volumes'][0]['State']


def ebs_modification_state(ebs_vol):
    print(f'Check volume modification state for: {ebs_vol}')
    ec2 = boto3.client('ec2', region_name=os.environ['AWS_REGION'])
    try:
        ebs_modification_status = ec2.describe_volumes_modifications(VolumeIds=[ebs_vol])['VolumesModifications'][0][
            'ModificationState'
        ]
    except:
        ebs_modification_status = None
    return ebs_modification_status


def get_backup_volume_id(stack_name):
    ec2 = boto3.client('ec2', region_name=os.environ['AWS_REGION'])
    volumes = ec2.describe_volumes(Filters=[{'Name': 'tag:Name', 'Values': [stack_name + "-backup"]}])
    print(("backup volumeId is: ", volumes['Volumes'][0]['VolumeId']))
    return volumes['Volumes'][0]['VolumeId']


def lambda_handler(event, context):
    ebs_vol = get_backup_volume_id(event['stack_name'])
    status = ebs_state(ebs_vol)
    if status == "error" or status == "deleted" or status == "deleting":
        event['ebsvolume_status'] = "Failed"
    elif status == "available" or status == "in-use":
        # if the volume is in-use, it's probably in use by the backmac node, so just try to proceed
        modification_status = ebs_modification_state(ebs_vol)
        if modification_status == "completed" or modification_status is None:
            event['ebsvolume_status'] = "Available"
        else:
            event['ebsvolume_status'] = modification_status
    else:
        # else use the waiter (status is most likely 'creating')
        event['ebsvolume_status'] = status
    return event


if __name__ == "__main__":
    '''
        simulate lambda env vars
        '''
    os.environ['AWS_LAMBDA_LOG_GROUP_NAME'] = "/aws/lambda/awsbackup_lambda"
    os.environ['AWS_LAMBDA_LOG_STREAM_NAME'] = '2017/09/10/[$LATEST]078e6de35eda4343a4d44002f646831e'
    os.environ['AWS_REGION'] = "us-east-2"

    event = {
        "dr_region": "us-east-1",
        "stack_name": "jira-stack",
        "security_group_name": "sg-abcdefgh",
        "backmac_instance": "i-00000000000000000",
        "efs_used_space": "2097172",
    }

    context = ''
    result = lambda_handler(event, context)
