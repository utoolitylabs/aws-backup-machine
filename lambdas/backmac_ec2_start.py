import boto3
import os


def get_exports():
    cfn = boto3.client('cloudformation', region_name=os.environ['AWS_REGION'])
    exports_dict = cfn.list_exports()
    return exports_dict


def start_ec2(instances):
    ec2 = boto3.client('ec2', region_name=os.environ['AWS_REGION'])
    response = ec2.start_instances(InstanceIds=[instances])
    return response


def get_tags(stack_name):
    cfn = boto3.client('cloudformation', region_name=os.environ['AWS_REGION'])
    tags = cfn.describe_stacks(StackName=stack_name)['Stacks'][0]['Tags']
    return tags


def lambda_handler(event, context):
    exports_dict = get_exports()
    try:
        backmac_instance = event['backmac_instance']
    except:
        backmac_dict = list(filter(lambda resource: resource['Name'] == 'BackmacNode', exports_dict['Exports']))
        backmac_instance = backmac_dict[0][u'Value']
    print(start_ec2(backmac_instance))
    event['stack_tags'] = get_tags(event['stack_name'])

    return event


if __name__ == "__main__":
    '''
        simulate lambda env vars
        '''
    os.environ['AWS_LAMBDA_LOG_GROUP_NAME'] = "/aws/lambda/awsbackup_lambda"
    os.environ['AWS_LAMBDA_LOG_STREAM_NAME'] = "2999/00/11/[$LATEST]000 dummy stream name 000"
    os.environ['AWS_REGION'] = "us-east-2"

    event = {"dr_region": "us-east-1", "stack_name": "jira-stack"}
    context = ''
    result = lambda_handler(event, context)
