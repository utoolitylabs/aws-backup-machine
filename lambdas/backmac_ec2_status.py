import boto3
import pprint
import sys
import os


def get_exports():
    cfn = boto3.client('cloudformation', region_name=os.environ['AWS_REGION'])
    exports_dict = cfn.list_exports()
    return exports_dict


def ec2_state(instance):
    print(f'Check instance state for: {instance}')
    ec2 = boto3.client('ec2', region_name=os.environ['AWS_REGION'])
    ec2_instance_status = ec2.describe_instance_status(InstanceIds=[instance])
    instance_state = ec2_instance_status['InstanceStatuses'][0]['InstanceState']['Name']
    instance_status = ec2_instance_status['InstanceStatuses'][0]['InstanceStatus']['Status']
    system_status = ec2_instance_status['InstanceStatuses'][0]['SystemStatus']['Status']
    return instance_state, instance_status, system_status


def lambda_handler(event, context):
    exports_dict = get_exports()
    try:
        backmac_instance = event['backmac_instance']
    except:
        backmac_dict = list(filter(lambda resource: resource['Name'] == 'BackmacNode', exports_dict['Exports']))
        backmac_instance = backmac_dict[0][u'Value']

    ec2_status, instance_status, system_status = ec2_state(backmac_instance)

    if ec2_status == 'running':
        if instance_status == 'ok' and system_status == 'ok':
            event['ec2_status'] = 'ready'
        elif instance_status == 'impaired' or system_status == 'impaired':
            event['ec2_status'] = 'failed'
        else:
            event['ec2_status'] = 'pending'
    elif ec2_status == 'pending':
        event['ec2_status'] = 'pending'
    else:
        # something is wrong - state is one of shutting-down, terminated, stopping or stopped
        event['ec2_status'] = 'failed'
        print(ec2_status)
    return event


if __name__ == "__main__":
    '''
        simulate lambda env vars
        '''
    os.environ['AWS_LAMBDA_LOG_GROUP_NAME'] = "/aws/lambda/awsbackup_lambda"
    os.environ['AWS_LAMBDA_LOG_STREAM_NAME'] = "2999/00/11/[$LATEST]000 dummy stream name 000"
    os.environ['AWS_REGION'] = "us-east-1"

    event = {"dr_region": "us-east-2", "stack_name": "jira-stack"}
    context = ''
    result = lambda_handler(event, context)
