import boto3
import pprint
import os


def ssm_cmd_check(backmac_instance, rsync_command_id):
    ssm = boto3.client('ssm', region_name=os.environ['AWS_REGION'])
    list_command = ssm.list_commands(CommandId=rsync_command_id)
    status = list_command['Commands'][0]['Status']
    result = ssm.get_command_invocation(CommandId=rsync_command_id, InstanceId=backmac_instance)
    if status == 'Success':
        pprint.pprint(result['StandardOutputContent'])
    return list_command['Commands'][0]['Status']


def lambda_handler(event, context):
    backmac_instance = event['backmac_instance']
    rsync_command_id = event['rsync_command_id']
    if rsync_command_id:
        event['rsync_status'] = ssm_cmd_check(backmac_instance, rsync_command_id)
    else:
        event['rsync_status'] = "Failed"
    print(('rsync status is: ' + event['rsync_status']))
    return event


if __name__ == "__main__":
    '''
        simulate lambda env vars
        '''
    os.environ['AWS_LAMBDA_LOG_GROUP_NAME'] = "/aws/lambda/awsbackup_lambda"
    os.environ['AWS_LAMBDA_LOG_STREAM_NAME'] = "2999/00/11/[$LATEST]000 dummy stream name 000"
    os.environ['AWS_REGION'] = "us-east-2"

    event = {
        "dr_region": "us-east-1",
        "stack_name": "jira-stack",
        "security_group_name": "sg-abcdefgh",
        "backmac_instance": "i-00000000000000000",
        "efs_used_space": "2097320",
        "ebs_backup_vol": "vol-00000000000000000",
        "rds_snap": "jira-stack-snap-000000000000",
        "rds_snap_arn": "arn:aws:rds:us-east-2:000000000000:snapshot:jira-stack-snap-000000000000",
        "rsync_command_id": "d77bf961-f9ca-470d-8dd1-000000000000",
    }
    context = ''
    result = lambda_handler(event, context)
    pprint.pprint(result)
