from botocore.vendored import requests
import os
import pprint


def build_json_data(message_type, message_text):
    message_type_dict = {
        "success": '{"type":"doc","version":1,"content":[{"type":"panel","attrs":{"panelType":"tip"},"content":[{"type":"paragraph","content":[{"type":"emoji","attrs": {"shortName":":successful:"}},{"type":"text","text":" #REPLACE#"}]}]}]}',
        "fail": '{"type":"doc","version":1,"content":[{"type":"panel","attrs":{"panelType":"warning"},"content":[{"type":"paragraph","content":[{"type":"emoji","attrs": {"shortName":":failed:"}},{"type":"text","text":" #REPLACE#"}]}]}]}',
    }
    json_data = message_type_dict[message_type].replace("#REPLACE#", message_text)
    return json_data


def send_result_to_stride(message_type, message_text):
    room_api_url = os.environ.get('room_api_url')
    room_token = os.environ.get('room_token')

    auth_token = 'Bearer ' + room_token
    headers = {'Content-Type': 'application/json', 'Authorization': auth_token}
    jsondata = build_json_data(message_type, message_text)

    try:
        connection = requests.post(room_api_url, data=jsondata, headers=headers)
    except requests.exceptions.RequestException as e:
        connection = e
        pprint.pprint(e)
    response = connection.text
    if 200 <= connection.status_code != 299:
        print(('notification failed: ' + str(connection.status_code) + ": " + response))
        pprint.pprint(response)
    else:
        pprint.pprint(response)
        pass
    return response, connection.status_code


def lambda_handler(event, context):
    if len(os.environ.get('room_api_url')) > 1 and len(os.environ.get('room_token')):
        try:
            stack_name = event['stack_name']
        except:
            stack_name = "Could not determine stack"
            pass
        try:
            error = event['Error']
        except:
            try:
                error = event['error']['Error']
            except:
                error = "No error message"
                pass
        try:
            cause = event['Cause']
        except:
            try:
                cause = event['error']['Cause'].translate(str.maketrans({"{": r"", "}": r""}))
            except:
                cause = "No error cause"
                pass
        message = f"Backup Machine iteration failed - {stack_name} - {error} - {cause[0:100]}"
        response, statusCode = send_result_to_stride("fail", message)
        if statusCode == 400:
            # bad request, probably json in the message
            try:
                send_result_to_stride(
                    "fail", f"Backup Machine iteration failed - {stack_name} : Look for errors in SFN logs"
                )
            except:
                # we can't send even a basic message to stride, give up
                pass
    return event


if __name__ == "__main__":
    '''
        simulate lambda env vars
        '''
    os.environ['AWS_LAMBDA_LOG_GROUP_NAME'] = "/aws/lambda/awsbackup_lambda"
    os.environ['AWS_LAMBDA_LOG_STREAM_NAME'] = "2999/00/11/[$LATEST]000 dummy stream name 000"
    os.environ['AWS_REGION'] = "us-east-2"
    os.environ['room_api_url'] = "mock"
    os.environ['room_token'] = "mock"

    event = {
        "Error": "Lambda.Unknown",
        "Cause": "The cause could not be determined because Lambda did not return an error type.",
    }
    context = ''
    result = lambda_handler(event, context)
    pprint.pprint(result)
