import boto3
import pprint
import os
import time


def get_exports():
    print('getting list of cloudformation exports')
    cfn = boto3.client('cloudformation', region_name=os.environ['AWS_REGION'])
    exports_dict = cfn.list_exports()
    return exports_dict


def add_security_group(backmac_instance, sg_name):
    print(f'Adding security group {sg_name} to backmac instance {backmac_instance}')
    ec2 = boto3.client('ec2', region_name=os.environ['AWS_REGION'])
    get_security_groups = ec2.describe_instance_attribute(Attribute='groupSet', InstanceId=backmac_instance)
    grouplist = [d['GroupId'] for d in get_security_groups['Groups']]
    if sg_name not in grouplist:
        grouplist.append(sg_name)
        type(grouplist)
        add_security_group = ec2.modify_instance_attribute(InstanceId=backmac_instance, Groups=grouplist)


def get_efs_filesystem_id(backmac_instance, efs_cname):
    if '.' not in efs_cname:
        filesystem_id = efs_cname
    else:
        efs = ssm_wait_response(backmac_instance, f"nslookup {efs_cname} | grep 'Name:' | awk '{{print $2}}'")
        filesystem_id = efs[1].split('.')[0]
    return filesystem_id


def get_used_space(efs_id):
    """adding used space from /media/atl to event for debugging and resizing if required"""
    # get disk space currently used on /media/atl (and make sure /media/atl is a separate mount point, not part of /).
    efs = boto3.client('efs', region_name=os.environ['AWS_REGION'])
    efs_details = efs.describe_file_systems(FileSystemId=efs_id)
    used_space = efs_details['FileSystems'][0]['SizeInBytes']
    return int(used_space['Value']/1000)


def ssm_wait_response(backmac_instance, cmd):
    ssm = boto3.client('ssm', region_name=os.environ['AWS_REGION'])
    ssm_command = ssm.send_command(
        InstanceIds=[backmac_instance],
        DocumentName='AWS-RunShellScript',
        Parameters={'commands': [cmd]},
        OutputS3BucketName='wpe-logs',
        OutputS3KeyPrefix='run-command-logs',
    )
    print(("for command: ", cmd, " command_Id is: ", ssm_command['Command']['CommandId']))
    status = 'Pending'
    while status == 'Pending' or status == 'InProgress':
        time.sleep(3)
        list_command = ssm.list_commands(CommandId=ssm_command['Command']['CommandId'])
        status = list_command['Commands'][0]['Status']
    result = ssm.get_command_invocation(CommandId=ssm_command['Command']['CommandId'], InstanceId=backmac_instance)
    return result['ResponseCode'], result['StandardOutputContent']


def check_mount_target(backmac_instance, efs_id):
    # TODO make sure the efs has a valid mount target in this AZ, if not, create one.
    efs = boto3.client('efs', region_name=os.environ['AWS_REGION'])
    targets = efs.describe_mount_targets(FileSystemId=efs_id)
    mac = ssm_wait_response(
        backmac_instance, 'curl -s http://169.254.169.254/latest/meta-data/network/interfaces/macs/'
    )
    backmac_subnet = ssm_wait_response(
        backmac_instance, f'curl -s http://169.254.169.254/latest/meta-data/network/interfaces/macs/{mac[1]}subnet-id'
    )
    print(backmac_subnet)
    mount_targets = []
    for target in targets['MountTargets']:
        print(target)
        mount_targets.append(target['SubnetId'])
    print(mount_targets)
    if backmac_subnet[1] in mount_targets:
        print('found subnet in mount targets')
    else:
        print("couldn't find subnet in mount targets, need to create")
        exit(1)
    pass


def lambda_handler(event, context):
    stack_name = event['stack_name']
    efs_cname_resource = stack_name + '-EFSCname'
    sg_name_resource = stack_name + '-SGname'
    exports_dict = get_exports()
    backmac_dict = [resource for resource in exports_dict['Exports'] if resource['Name'] == 'BackmacNode']
    print(backmac_dict)
    backmac_instance = backmac_dict[0]['Value']
    print(backmac_instance)
    efs_cname_dict = [resource for resource in exports_dict['Exports'] if resource['Name'] == efs_cname_resource]
    efs_cname = efs_cname_dict[0]['Value']
    event['efs_cname'] = efs_cname
    efs_id = get_efs_filesystem_id(backmac_instance, efs_cname)
    event['efs_id'] = efs_id
    sg_name_dict = [resource for resource in exports_dict['Exports'] if resource['Name'] == sg_name_resource]
    sg_name = sg_name_dict[0]['Value']
    check_mount_target(backmac_instance, efs_id)
    event['security_group_name'] = sg_name
    event['backmac_instance'] = backmac_instance
    add_security_group(backmac_instance, sg_name)
    print(f'Mounting efs {efs_id} to backmac instance {backmac_instance}')
    mount_cmd = f'mount -t efs {efs_id}:/ /media/atl'
    umount_cmd = 'umount -f /media/atl'
    ssm = boto3.client('ssm', region_name=os.environ['AWS_REGION'])
    umount = ssm.send_command(
        InstanceIds=[backmac_instance],
        DocumentName='AWS-RunShellScript',
        Parameters={'commands': [umount_cmd]},
        OutputS3BucketName='wpe-logs',
        OutputS3KeyPrefix='run-command-logs',
    )
    print(f"umount command_id is: {umount['Command']['CommandId']}")
    time.sleep(5)
    mount = ssm.send_command(
        InstanceIds=[backmac_instance],
        DocumentName='AWS-RunShellScript',
        Parameters={'commands': [mount_cmd]},
        OutputS3BucketName='wpe-logs',
        OutputS3KeyPrefix='run-command-logs',
    )
    cmd_id = mount['Command']['CommandId']
    print(f"mount command_id is: {mount['Command']['CommandId']}")
    time.sleep(5)
    cmd_state = ssm.get_command_invocation(CommandId=cmd_id, InstanceId=backmac_instance)
    print((cmd_state['StandardOutputContent']))
    print((cmd_state['StandardErrorContent']))
    event['efs_used_space'] = get_used_space(efs_id)
    return event


if __name__ == "__main__":
    '''
        simulate lambda env vars
        '''
    os.environ['AWS_LAMBDA_LOG_GROUP_NAME'] = "/aws/lambda/awsbackup_lambda"
    os.environ['AWS_LAMBDA_LOG_STREAM_NAME'] = "2999/00/11/[$LATEST]000 dummy stream name 000"
    os.environ['AWS_REGION'] = "us-east-1"

    event = {"dr_region": "us-west-2", "stack_name": "jira-stack"}

    context = ''
    result = lambda_handler(event, context)
    pprint.pprint(result)
