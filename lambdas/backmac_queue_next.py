import boto3
import pprint
import os


def lambda_handler(event, context):
    dr_region = os.environ['dr_region']
    sfn = boto3.client('stepfunctions', region_name=os.environ['AWS_REGION'])
    sfn_run = sfn.start_execution(
        stateMachineArn=f"arn:aws:states:{os.environ['AWS_REGION']}:{boto3.client('sts').get_caller_identity().get('Account')}:stateMachine:backup_machine",
        input=f'{{"dr_region": "{dr_region}"}}',
    )
    print(f"Next backup run queued at {sfn_run[u'startDate']}")
    return event


if __name__ == "__main__":
    '''
        simulate lambda env vars
        '''
    os.environ['AWS_LAMBDA_LOG_GROUP_NAME'] = "/aws/lambda/awsbackup_lambda"
    os.environ['AWS_LAMBDA_LOG_STREAM_NAME'] = "2999/00/11/[$LATEST]000 dummy stream name 000"
    os.environ['AWS_REGION'] = "us-east-2"

    event = {"dr_region": "us-east-1"}
    context = ''
    result = lambda_handler(event, context)
    pprint.pprint(result)
