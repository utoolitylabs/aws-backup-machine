import boto3
import pprint
import os


def ensure_drsnap_complete(dr_snap, dr_region):
    print(f'Checking if snapshot complete at {dr_region} for: {dr_snap}')
    rds = boto3.client('rds', region_name=dr_region)
    try:
        dr_snapshot = rds.describe_db_snapshots(DBSnapshotIdentifier=dr_snap)
    except Exception as e:
        print('type is:', e.__class__.__name__)
        pprint.pprint(e)
        template = "An exception of type {0} occurred. Arguments:\n{1!r}"
        message = template.format(type(e).__name__, e.args)
        return 'failed'
    return dr_snapshot['DBSnapshots'][0]['Status']


def lambda_handler(event, context):
    dr_region = event['dr_region']
    dr_snap = event['rds_dr_snap']
    event['rds2dr_status'] = 'waiting'
    dr_snap_state = ensure_drsnap_complete(dr_snap, dr_region)
    event['rds2dr_status'] = dr_snap_state
    print('returning successfully')
    return event


if __name__ == "__main__":
    '''
        simulate lambda env vars
        '''
    os.environ['AWS_LAMBDA_LOG_GROUP_NAME'] = "/aws/lambda/awsbackup_lambda"
    os.environ['AWS_LAMBDA_LOG_STREAM_NAME'] = '2017/09/10/[$LATEST]078e6de35eda4343a4d44002f646831e'
    os.environ['AWS_REGION'] = "us-east-2"

    event = {
        "ebs_backup_vol": "vol-00000000000000000",
        "dr_region": "us-east-1",
        "stack_name": "jira-stack",
        "rds_snap_arn": "arn:aws:rds:us-west-2:000000000000:snapshot:jira-stack-snap-000000000000",
        "rds_snap": "jira-stack-snap-000000000000",
        "ebs_snap": "snap-00000000000000000",
        "rds_dr_snap": "dr-jira-stack-snap-000000000000",
    }
    context = ''
    result = lambda_handler(event, context)
    pprint.pprint(result)
