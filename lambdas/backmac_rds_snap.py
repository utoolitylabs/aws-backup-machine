import boto3
import pprint
import os
import time

from datetime import datetime, timedelta


def get_backup_retention(stack_name):
    cfn = boto3.client('cloudformation', region_name=os.environ['AWS_REGION'])
    tags = cfn.describe_stacks(StackName=stack_name)['Stacks'][0]['Tags']
    backmac_retention = 30
    for tag in tags:
        if tag['Key'] == 'backmac_retention_days':
            backmac_retention = tag['Value']
    try:
        backmac_retention = int(backmac_retention)
    except ValueError:
        print(f'The value found for backmac_retention_days ({backmac_retention}) is not an integer')
        exit(1)
    return backmac_retention


def lambda_handler(event, context):
    stack_name = event['stack_name']
    event['backup_retention'] = get_backup_retention(stack_name)
    timestamp = time.strftime('%Y%m%d%H%M')
    rds_snap_name = stack_name + '-snap-' + timestamp
    rds = boto3.client('rds', region_name=os.environ['AWS_REGION'])
    tags = [
        {'Key': 'Name', 'Value': stack_name + '-rds-snap-' + timestamp},
        {'Key': 'backup_lambda_log_group_name', 'Value': '{}'.format(os.environ['AWS_LAMBDA_LOG_GROUP_NAME'])},
        {
            'Key': 'backup_lambda_log_stream_name',
            'Value': '{}'.format(os.environ['AWS_LAMBDA_LOG_STREAM_NAME'].replace('/[$LATEST]', '-LATEST-')),
        },
        {'Key': 'backup_date', 'Value': '{}'.format(datetime.now())},
        {'Key': 'backup_delete_after', 'Value': '{}'.format(datetime.now() + timedelta(event['backup_retention']))},
    ]
    tags = tags + event['stack_tags']
    rds_snap = rds.create_db_snapshot(DBInstanceIdentifier=stack_name, DBSnapshotIdentifier=rds_snap_name, Tags=tags)
    # add rds snapshot_id to event
    event['rds_snap'] = rds_snap['DBSnapshot']['DBSnapshotIdentifier']
    event['rds_snap_arn'] = rds_snap['DBSnapshot']['DBSnapshotArn']
    return event


if __name__ == "__main__":
    '''
        simulate lambda env vars
        '''
    os.environ['AWS_LAMBDA_LOG_GROUP_NAME'] = "/aws/lambda/awsbackup_lambda"
    os.environ['AWS_LAMBDA_LOG_STREAM_NAME'] = "2999/00/11/[$LATEST]000 dummy stream name 000"
    os.environ['AWS_REGION'] = "us-east-2"

    event = {
        "dr_region": "us-east-1",
        "stack_name": "jira-stack",
        "security_group_name": "sg-abcdefgh",
        "backmac_instance": "i-00000000000000000",
        "efs_used_space": "2097320",
        "ebs_backup_vol": "vol-00000000000000000",
    }
    context = ''
    result = lambda_handler(event, context)
    pprint.pprint(result)
