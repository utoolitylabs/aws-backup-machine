from botocore.vendored import requests
import os
import pprint


def build_json_data(message_type, message_text):
    message_type_dict = {
        "success": '{"type":"doc","version":1,"content":[{"type":"panel","attrs":{"panelType":"tip"},"content":[{"type":"paragraph","content":[{"type":"text","text":" #REPLACE#"}]}]}]}',
        "warning": '{"type":"doc","version":1,"content":[{"type":"panel","attrs":{"panelType":"warning"},"content":[{"type":"paragraph","content":[{"type":"text","text":" #REPLACE#"}]}]}]}',
        "fail": '{"type":"doc","version":1,"content":[{"type":"panel","attrs":{"panelType":"error"},"content":[{"type":"paragraph","content":[{"type":"text","text":" #REPLACE#"}]}]}]}',
    }
    json_data = message_type_dict[message_type].replace("#REPLACE#", message_text)
    return json_data


def send_result_to_stride(message_type, message_text):
    room_api_url = os.environ.get('room_api_url')
    room_token = os.environ.get('room_token')

    auth_token = 'Bearer ' + room_token
    headers = {'Content-Type': 'application/json', 'Authorization': auth_token}
    jsondata = build_json_data(message_type, message_text)

    try:
        connection = requests.post(room_api_url, data=jsondata, headers=headers)
    except requests.exceptions.RequestException as e:
        connection = e
        pprint.pprint(e)
    response = connection.text
    if 200 <= connection.status_code != 299:
        print(('notification failed: ' + str(connection.status_code) + ": " + response))
        pprint.pprint(response)
    else:
        pprint.pprint(response)
        return ()


def lambda_handler(event, context):
    stack_name = event['stack_name']
    message = f"Backup Machine requeueing {stack_name} for retry in {os.environ['AWS_REGION']}"
    try:
        send_result_to_stride("warning", message)
    except:
        # stride not configured or sending message failed, not fatal.
        pass
    return event


if __name__ == "__main__":
    '''
        simulate lambda env vars
        '''
    os.environ['AWS_LAMBDA_LOG_GROUP_NAME'] = "/aws/lambda/awsbackup_lambda"
    os.environ['AWS_LAMBDA_LOG_STREAM_NAME'] = "2999/00/11/[$LATEST]000 dummy stream name 000"
    os.environ['AWS_REGION'] = "us-east-2"

    event = [
        {
            "backmac_instance": "i-00000000000000000",
            "ebs_backup_vol": "vol-00000000000000000",
            "dr_region": "us-east-1",
            "stack_name": "jira-stack",
            "security_group_name": "sg-abcdefgh",
            "rds2dr_status": "waiting",
            "rds_snap_arn": "arn:aws:rds:us-west-2:000000000000:snapshot:jira-stack-snap-000000000000",
            "ebs_dr_snap": "snap-00000000000000000",
            "ebs2dr_status": "completed",
            "rds_snap": "jira-stack-snap-000000000000",
            "ebs_snap": "snap-00000000000000000",
            "rsync_status": "Success",
            "rsync_command_id": "836f6970-0fe1-4e10-a233-000000000000",
        }
    ]
    context = ''
    result = lambda_handler(event, context)
    pprint.pprint(result)
