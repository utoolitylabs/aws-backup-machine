import boto3
import os
import pprint
import time


def resize_backup_volume(backmac_instance, stack_name, efs_size, volume_id):
    ec2 = boto3.client('ec2', region_name=os.environ['AWS_REGION'])
    zone = ssm_wait_response(
        backmac_instance, 'curl -s http://169.254.169.254/latest/meta-data/placement/availability-zone'
    )
    volumesize = float(efs_size) / 1073742  # current disk space used in GiB(ish)
    # let's not go crazy with adding our 20% disk space buffer
    if (volumesize * 0.2) > 200:  # GiB
        volumesize = volumesize + 200
    else:
        volumesize = volumesize * 1.2
    # gp2 has constraints of 1-16384, so this might still fail if volumesize * 1.2 < 0.5GiB , so adding +5
    response = ec2.modify_volume(VolumeId=volume_id, Size=int(round(volumesize + 5)), VolumeType='gp2')
    return response


def e2fsck_ebs_volume(backmac_instance, device):
    # this can take a while (~4 min for 3TB volumes)
    response = ssm_wait_response(backmac_instance, f'e2fsck -f {device}')
    return response


def ssm_wait_response(backmac_instance, cmd):
    ssm = boto3.client('ssm', region_name=os.environ['AWS_REGION'])
    ssm_command = ssm.send_command(
        InstanceIds=[backmac_instance],
        DocumentName='AWS-RunShellScript',
        Parameters={'commands': [cmd]},
        OutputS3BucketName='wpe-logs',
        OutputS3KeyPrefix='run-command-logs',
    )
    print('for command: ', cmd, ' command_Id is: ', ssm_command['Command']['CommandId'])
    status = 'Pending'
    while status == 'Pending' or status == 'InProgress':
        time.sleep(3)
        list_command = ssm.list_commands(CommandId=ssm_command['Command']['CommandId'])
        status = list_command['Commands'][0]['Status']
    result = ssm.get_command_invocation(CommandId=ssm_command['Command']['CommandId'], InstanceId=backmac_instance)
    return result['ResponseCode'], result['StandardOutputContent']


def lambda_handler(event, context):
    response = resize_backup_volume(
        event['backmac_instance'], event['stack_name'], event['efs_used_space'], event['ebs_backup_vol']
    )
    print(response)
    e2fsck_ebs_volume(event['backmac_instance'], event['backup_vol_device_name'])
    return event


if __name__ == "__main__":
    '''
        simulate lambda env vars
        '''
    os.environ['AWS_LAMBDA_LOG_GROUP_NAME'] = "/aws/lambda/awsbackup_lambda"
    os.environ['AWS_LAMBDA_LOG_STREAM_NAME'] = "2999/00/11/[$LATEST]000 dummy stream name 000"
    os.environ['AWS_REGION'] = "us-east-2"

    event = {
        "stack_name": "jira-stack",
        "backmac_instance": "i-00000000000000000",
        "dr_region": "us-east-1",
        "security_group_name": "sg-abcdefgh",
        "efs_used_space": "2097172",
    }

    context = ''
    result = lambda_handler(event, context)
    pprint.pprint(result)
