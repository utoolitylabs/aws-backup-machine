import boto3
import pprint
import os
from botocore.vendored import requests


def build_json_data(message_type, message_text):
    message_type_dict = {
        "success": '{"type":"doc","version":1,"content":[{"type":"panel","attrs":{"panelType":"tip"},"content":[{"type":"paragraph","content":[{"type":"text","text":" #REPLACE#"}]}]}]}',
        "fail": '{"type":"doc","version":1,"content":[{"type":"panel","attrs":{"panelType":"error"},"content":[{"type":"paragraph","content":[{"type":"text","text":" #REPLACE#"}]}]}]}',
    }
    json_data = message_type_dict[message_type].replace("#REPLACE#", message_text)
    return json_data


def send_result_to_stride(message_type, message_text):
    room_api_url = os.environ.get('room_api_url')
    room_token = os.environ.get('room_token')

    auth_token = 'Bearer ' + room_token
    headers = {'Content-Type': 'application/json', 'Authorization': auth_token}
    jsondata = build_json_data(message_type, message_text)

    try:
        connection = requests.post(room_api_url, data=jsondata, headers=headers)
    except requests.exceptions.RequestException as e:
        connection = e
        pprint.pprint(e)
    response = connection.text
    if 202 <= connection.status_code != 299:
        print(('notification failed: ' + str(connection.status_code) + ": " + response))
        pprint.pprint(response)
    else:
        pprint.pprint(response)
        return ()


def get_instances_to_backup():
    import boto3

    cfn = boto3.resource('cloudformation', region_name=os.environ['AWS_REGION'])
    backup_stack_list = []
    for stack in list(cfn.stacks.all()):
        print(f'Checking: {stack}')
        if {'Key': 'backmac_enabled', 'Value': 'true'} in stack.tags:
            print(f'Adding: {stack} to queue for backup')
            backup_stack_list.append(stack.name)
    return backup_stack_list


def push_backups_to_queue():
    stack_list = get_instances_to_backup()
    sqs = boto3.client('sqs', region_name=os.environ['AWS_REGION'])
    # add stacks in stack_list to the queue to be backed up
    dedup = 0
    for stack in stack_list:
        dedup += 1
        try:
            queue_response = sqs.send_message(
                QueueUrl=f"https://sqs.{os.environ['AWS_REGION']}.amazonaws.com/{boto3.client('sts').get_caller_identity().get('Account')}/backmac.fifo",
                MessageBody=stack,
                MessageGroupId='backup_these_stacks',
                MessageDeduplicationId=str(dedup),
            )
            print(queue_response)
        except Exception as e:
            print('type is:', e.__class__.__name__)
            pprint.pprint(e)
            template = "An exception of type {0} occurred. Arguments:\n{1!r}"
            message = template.format(type(e).__name__, e.args)
            return 'failed'


def start_backup_run(dr_region):
    # start stepfunction
    sfn = boto3.client('stepfunctions', region_name=os.environ['AWS_REGION'])
    sfn_run = sfn.start_execution(
        stateMachineArn=f"arn:aws:states:{os.environ['AWS_REGION']}:{boto3.client('sts').get_caller_identity().get('Account')}:stateMachine:backup_machine",
        input=f'{{"dr_region": "{dr_region}"}}',
    )
    return sfn_run


def lambda_handler(event, context):
    try:
        send_result_to_stride("success", f"Backmac run STARTED in {os.environ['AWS_REGION']}")
    except:
        # stride not configured or sending message failed, not fatal.
        pass
    event['dr_region'] = os.environ['dr_region']
    push_backups_to_queue()
    # start backp run
    print(start_backup_run(event['dr_region']))
    return event


if __name__ == "__main__":
    '''
        simulate lambda env vars
        '''
    os.environ['AWS_LAMBDA_LOG_GROUP_NAME'] = "/aws/lambda/awsbackup_lambda"
    os.environ['AWS_LAMBDA_LOG_STREAM_NAME'] = "2999/00/11/[$LATEST]000 dummy stream name 000"
    os.environ['AWS_REGION'] = "us-east-2"
    os.environ['dr_region'] = "us-east-1"

    event = {"dr_region": "us-east-2"}
    context = ''
    result = lambda_handler(event, context)
    pprint.pprint(result)
