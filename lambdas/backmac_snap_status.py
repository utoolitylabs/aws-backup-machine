import boto3
import pprint
import sys
import os


def rdssnap_state(rds_snap):
    print(f'Check snapshot state for: {rds_snap}')
    rds = boto3.client('rds', region_name=os.environ['AWS_REGION'])
    db_snapshot_status = rds.describe_db_snapshots(DBSnapshotIdentifier=rds_snap)
    return db_snapshot_status['DBSnapshots'][0]['Status']


def ebssnap_state(ebs_snap):
    print(f'Check snapshot state for: {ebs_snap}')
    ec2 = boto3.client('ec2', region_name=os.environ['AWS_REGION'])
    ebs_snapshot_status = ec2.describe_snapshots(SnapshotIds=[ebs_snap])
    return ebs_snapshot_status['Snapshots'][0]['State']


def lambda_handler(event, context):
    rds_snap = event['rds_snap']
    ebs_snap = event['ebs_snap']
    rdsdnap_status = rdssnap_state(rds_snap)
    ebssnap_status = ebssnap_state(ebs_snap)

    if rdsdnap_status == 'available' and ebssnap_status == 'completed':
        event['snapshot_status'] = 'complete'
    else:
        event['snapshot_status'] = 'incomplete'
    print('returning successfully')
    return event


if __name__ == "__main__":
    '''
        simulate lambda env vars
        '''
    os.environ['AWS_LAMBDA_LOG_GROUP_NAME'] = "/aws/lambda/awsbackup_lambda"
    os.environ['AWS_LAMBDA_LOG_STREAM_NAME'] = '2017/09/10/[$LATEST]078e6de35eda4343a4d44002f646831e'
    os.environ['AWS_REGION'] = "us-east-2"

    event = {
        "backmac_instance": "i-00000000000000000",
        "ebs_backup_vol": "vol-00000000000000000",
        "dr_region": "us-east-1",
        "stack_name": "jira-stack",
        "security_group_name": "sg-abcdefgh",
        "rds_snap_arn": "arn:aws:rds:us-west-2:000000000000:snapshot:jira-stack-snap-000000000000",
        "rds_snap": "jira-stack-snap-000000000000",
        "ebs_snap": "snap-00000000000000000",
        "rsync_status": "Success",
        "rsync_command_id": "a9d60c64-c9ee-4152-942e-000000000000",
    }
    context = ''
    result = lambda_handler(event, context)
    try:
        pprint.pprint(result)
    except Exception as e:
        print(('type is:', e.__class__.__name__))
        pprint.pprint(e)
        template = "An exception of type {0} occurred. Arguments:\n{1!r}"
        message = template.format(type(e).__name__, e.args)
        sys.exit(message)
