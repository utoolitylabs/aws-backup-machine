#!/bin/sh

# $1 = region
# $2 = bucket/folder (no s3:// prefix or trailing slash)

# e.g. ./package.sh us-east-1 my-awesome-bucket/test
set +x
region=${1:-us-east-1}

cd lambdas
find . -name \*.py -exec zip {}.zip {} \;
cd ../

if [[ -z "${CI_ACCOUNT_ID}" ]]; then
    aws sts get-caller-identity >/dev/null 2>&1
    if [ $? -eq 255 ]; then
      echo "Tried to find AWS_ACCOUNT_ID from aws sts get-caller-identity but failed. You may need to run cloudtoken or aws configure."
      exit 255
    else
        awsaccount=`aws sts get-caller-identity | grep Account | awk -F'"' {'print $4'}`
    fi
else
    awsaccount=${CI_ACCOUNT_ID}
fi

r=${region}
bucket=${2:-wpe-backmac-deployment-${awsaccount}-${r//\"/}}
bucket="s3://${bucket}"

find scripts -type f -exec aws s3 cp {} ${bucket}/ \;
find lambdas -type f -name \*.zip -exec aws s3 cp {} ${bucket}/ \;
find templates -type f -name \*.yaml -exec aws s3 cp {} ${bucket}/ \;

rm lambdas/*.zip
echo "Done :yay:"
